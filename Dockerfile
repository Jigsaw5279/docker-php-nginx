FROM php:8-fpm-alpine3.13

LABEL author="Fabian Bettag <fabianbettag@gmail.com>"
LABEL description="Base Image for PHP applications in a Laravel context"

RUN apk update \
    && apk add nginx zip unzip git sqlite \
    && adduser -D -u 1000 -g 'www' www \
    && mkdir /www \
    && chown -R www:www /var/lib/nginx \
    && chown -R www:www /www \
    && rm -rf /etc/nginx/nginx.conf
    
ENV PHP_FPM_USER="www"
ENV PHP_FPM_GROUP="www"
ENV PHP_FPM_LISTEN_MODE="0660"
ENV PHP_MEMORY_LIMIT="512M"
ENV PHP_MAX_UPLOAD="50M"
ENV PHP_MAX_FILE_UPLOAD="200"
ENV PHP_MAX_POST="100M"
ENV PHP_DISPLAY_ERRORS="Off"
ENV PHP_DISPLAY_STARTUP_ERRORS="Off"
ENV PHP_ERROR_REPORTING="E_COMPILE_ERROR\|E_RECOVERABLE_ERROR\|E_ERROR\|E_CORE_ERROR"
ENV PHP_CGI_FIX_PATHINFO=0
ENV TIMEZONE="Europe/Berlin"

RUN apk add curl \
    tzdata \
    npm

RUN docker-php-ext-install \
    bcmath \
    pdo_mysql

RUN sed -i "s|;listen.owner\s*=\s*nobody|listen.owner = ${PHP_FPM_USER}|g" /usr/local/etc/php-fpm.conf \
    && sed -i "s|;listen.group\s*=\s*nobody|listen.group = ${PHP_FPM_GROUP}|g" /usr/local/etc/php-fpm.conf \
    && sed -i "s|;listen.mode\s*=\s*0660|listen.mode = ${PHP_FPM_LISTEN_MODE}|g" /usr/local/etc/php-fpm.conf \
    && sed -i "s|user\s*=\s*www-data|user = ${PHP_FPM_USER}|g" /usr/local/etc/php-fpm.d/www.conf \
    && sed -i "s|group\s*=\s*www-data|group = ${PHP_FPM_GROUP}|g" /usr/local/etc/php-fpm.d/www.conf \
    && sed -i "s|;log_level\s*=\s*notice|log_level = notice|g" /usr/local/etc/php-fpm.conf

RUN cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini \
    && sed -i "s|display_errors\s*=\s*Off|display_errors = ${PHP_DISPLAY_ERRORS}|i" /usr/local/etc/php/php.ini \
    && sed -i "s|display_startup_errors\s*=\s*Off|display_startup_errors = ${PHP_DISPLAY_STARTUP_ERRORS}|i" /usr/local/etc/php/php.ini \
    && sed -i "s|error_reporting\s*=\s*E_ALL & ~E_DEPRECATED & ~E_STRICT|error_reporting = ${PHP_ERROR_REPORTING}|i" /usr/local/etc/php/php.ini \
    && sed -i "s|;*memory_limit =.*|memory_limit = ${PHP_MEMORY_LIMIT}|i" /usr/local/etc/php/php.ini \
    && sed -i "s|;*upload_max_filesize =.*|upload_max_filesize = ${PHP_MAX_UPLOAD}|i" /usr/local/etc/php/php.ini \
    && sed -i "s|;*max_file_uploads =.*|max_file_uploads = ${PHP_MAX_FILE_UPLOAD}|i" /usr/local/etc/php/php.ini \
    && sed -i "s|;*post_max_size =.*|post_max_size = ${PHP_MAX_POST}|i" /usr/local/etc/php/php.ini \
    && sed -i "s|;*cgi.fix_pathinfo=.*|cgi.fix_pathinfo= ${PHP_CGI_FIX_PATHINFO}|i" /usr/local/etc/php/php.ini

RUN rm -rf /etc/localtime \
    && ln -s /usr/share/zoneinfo/${TIMEZONE} /etc/localtime \
    && echo "${TIMEZONE}" > /etc/timezone \
    && sed -i "s|;*date.timezone =.*|date.timezone = ${TIMEZONE}|i" /usr/local/etc/php/php.ini
    #&& echo 'sendmail_path = "/usr/sbin/ssmtp -t "' > /etc/php8/conf.d/mail.ini

EXPOSE 80

COPY nginx.conf /etc/nginx/nginx.conf
COPY start_nginx.sh /start_nginx.sh
COPY start_php-fpm.sh /start_php-fpm.sh
COPY wrapper.sh /wrapper.sh
COPY install_composer.sh /install_composer.sh
COPY entrypoint.sh /entrypoint.sh
COPY enable_xdebug.sh /enable_xdebug.sh
COPY disable_xdebug.sh /disable_xdebug.sh

RUN chmod +x /start_nginx.sh \
    /start_php-fpm.sh \
    /wrapper.sh \
    /install_composer.sh \
    /entrypoint.sh \
    /enable_xdebug.sh \
    /disable_xdebug.sh

RUN /install_composer.sh

CMD ["sh", "/wrapper.sh"]

COPY index.php /www/public/index.php

RUN chown -R www:www /www
RUN chown -R www:www /var/log
RUN chown -R www:www /var/tmp

RUN rm -f /var/cache/apk/*
