#!/bin/sh
set -e

echo "Installing composer"
EXPECTED_SIGNATURE="$(curl -q https://composer.github.io/installer.sig)"
curl -sS https://getcomposer.org/installer > composer-setup.php
ACTUAL_SIGNATURE="$(php -r "echo hash_file('SHA384', 'composer-setup.php');")"

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    >&2 echo 'ERROR: Invalid installer signature'
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --quiet --install-dir=/usr/bin --filename=composer
RESULT=$?
rm composer-setup.php
composer --version
exit $RESULT
