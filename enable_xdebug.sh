#!/bin/sh

set -e

sed -i '/xdebug/s/^;//g' /etc/php8/conf.d/xdebug.ini

ps axf | grep "php-fpm: master process" | grep -v grep | awk '{print "kill -USR2 " $1}' | sh

echo "XDebug enabled"