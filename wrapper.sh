#!/bin/sh

echo "Starting PHP-FPM"

/start_php-fpm.sh -D
status=$?
if [ $status -ne 0 ]; then
  echo "php-fpm7 Failed: $status"
  exit $status
  else echo "Starting PHP-FPM: OK"
fi

sleep 1

/entrypoint.sh

/start_nginx.sh -D
status=$?
if [ $status -ne 0 ]; then
  echo "Nginx Failed: $status"
  exit $status
  else echo "Starting Nginx: OK"
fi

sleep 1

while /bin/true; do
  ps aux | grep 'php-fpm: master process' | grep -q -v grep
  PHP_FPM_STATUS=$?

  ps aux | grep 'nginx: master process' | grep -q -v grep
  NGINX_STATUS=$?

  if [ $PHP_FPM_STATUS -ne 0 ];
    then
      echo "$(date +%F_%T) FATAL: PHP-FPM Raised a Status Code of $PHP_FPM_STATUS and exited"
      exit -1

   elif [ $NGINX_STATUS -ne 0 ];
     then
       echo "$(date +%F_%T) FATAL: NGINX Raised a Status Code of $NGINX_STATUS and exited"
       exit -1
  fi
  sleep 5
done